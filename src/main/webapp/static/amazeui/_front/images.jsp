<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
    content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">
<link rel="stylesheet" href="/static/amazeui/css/amazeui.min.css">
<title>照片墙</title>
</head>
<body>
    <figure data-am-widget="figure" class="am am-figure am-figure-default "   data-am-figure="{  pureview: 'true' }">
    


      <img src="http://s.amazeui.org/media/i/demos/pure-1.jpg?imageView2/0/w/640" data-rel="http://s.amazeui.org/media/i/demos/pure-1.jpg" alt="春天的花开秋天的风以及冬天的落阳"/>
          <figcaption class="am-figure-capition-btm">
            春天的花开秋天的风以及冬天的落阳
          </figcaption>

    
  </figure>
  

<script type="text/javascript" src="/static/amazeui/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/amazeui/js/amazeui.min.js"></script>
</body>
</html>